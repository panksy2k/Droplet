package com.pardasani.digital;

import com.pardasani.digital.common.config.MongoDBConfiguration;
import com.pardasani.digital.common.config.SecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({MongoDBConfiguration.class, SecurityConfig.class})
public class MediaManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(MediaManagementApplication.class, args);
	}
}
