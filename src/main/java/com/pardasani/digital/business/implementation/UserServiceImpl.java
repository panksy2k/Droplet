package com.pardasani.digital.business.implementation;

import com.pardasani.digital.business.UserService;
import com.pardasani.digital.domain.DropletUser;
import com.pardasani.digital.exception.MediaManagementException;
import com.pardasani.digital.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Integer registerUser(DropletUser dropletUser) {
        DropletUser existingDropletUser = userRepository.findByUserNameEmail(dropletUser.getUserNameEmail());
        if (null != existingDropletUser)
            throw new MediaManagementException("DropletUser / Email is already registered");

        existingDropletUser = userRepository.save(dropletUser);
        return existingDropletUser.getId();
    }

    @Override
    public DropletUser changeUserDetails(DropletUser oldDropletUser) {
        return userRepository.updateUserDetails(oldDropletUser);
    }

    @Override
    public List<DropletUser> findAllUsers() {
        return userRepository.findAllUsers();
    }

    @Override
    public DropletUser getUserByUsername(String userEmail) {
        return userRepository.findByUserNameEmail(userEmail);
    }
}
