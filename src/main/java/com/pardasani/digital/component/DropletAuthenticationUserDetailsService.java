package com.pardasani.digital.component;

import com.pardasani.digital.domain.DropletUser;
import com.pardasani.digital.exception.MediaManagementException;
import com.pardasani.digital.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * Created by pankajpardasani on 06/04/2016.
 */

@Component
public class DropletAuthenticationUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;

        DropletUser dbUser = getDropletUser(userName);

        if (null == dbUser) {
            throw new UsernameNotFoundException("UserDetailsService returned null, which is an interface contract violation");
        }

        UserDetails userdetails = new User(dbUser.getUserNameEmail(),
                dbUser.getUserPassword(),
                enabled,
                accountNonExpired,
                credentialsNonExpired,
                accountNonLocked,
                dbUser.getRoles());

        return userdetails;
    }

    private DropletUser getDropletUser(String userName) {
        try {
            return userRepository.findByUserNameEmail(userName);
        } catch (Exception repositoryProblem) {
            throw new MediaManagementException("Error while fetching user details from DB", repositoryProblem);
        }
    }
}
